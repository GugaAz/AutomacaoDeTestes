/// <reference types = "cypress"/>

import Login from "../support/Desafio/Pages/Login"

describe ('teste pagina de login', () => {
//acessa a URL e realiza asserção     
beforeEach( () =>{
    Login.acessarPagina();
    cy.get('.orangehrm-login-branding > img').should('be.visible');

})
it('realizar login', () => {
    Login.acessarPagina();
    Login.realizarLogin('Admin','admin123');
    cy.get('.oxd-userdropdown-img').should('be.visible');
})

it('realizar login com username errado', () => {
    Login.acessarPagina();
    Login.realizarLogin('administrador','admin123');
    cy.get('.oxd-alert-content > .oxd-text').should('be.visible');
})

it('realizar login com password errado', () => {
    Login.acessarPagina();
    Login.realizarLogin('Admin','administrador');
    cy.get('.oxd-alert-content > .oxd-text').should('be.visible');
})

})