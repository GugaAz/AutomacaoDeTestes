//Metodos de ações da página Recruitment
const el= require('./elements').ELEMENTS;

class Recruitment {
    acessarPaginaRecruitment(){
        cy.get(el.botaoPagRecruitment).click();
    }
    acessarRecruitmentVagas(){
        cy.get(el.botaoRecruitmentVagas).click();
    }

    procurarCandidato( VacancyName, HiringManager, Status, CandidateName, Keywords,dataInicial,dataFinal, Method){
        cy.get(el.botaoDropdownVacancyC).click({force:true});
        cy.get(el.dropdownCandidates).contains(VacancyName).click();
        cy.get(el.botaoDropdownHiringC).click({force:true});
        cy.get(el.dropdownCandidates).contains(HiringManager).click()
        cy.get(el.botaoDropdownStatusC).click({force:true});
        cy.get(el.dropdownCandidates).contains(Status).click();
        cy.get(el.campoCandidateName).type(CandidateName);
        cy.wait(1200);
        cy.get(el.dropdownCandidateName).contains(CandidateName).click();
        cy.get(el.campoKeywords).type(Keywords);
        cy.get(el.campoApplicationDataInitial).type(dataInicial);
        cy.get(el.campoApplicationDataFinal).type(dataFinal);
        cy.get(el.botaoDropdownMethod).click({force:true});
        cy.get(el.dropdown).contains(Method).click();
        cy.get(el.botaoSearchCandidate).click({force:true});
        //Se o candidato não for encontrado, a asserção passa
        cy.get('.orangehrm-horizontal-padding > .oxd-text').should('have.text', 'No Records Found')
    

    }
    
    
    
    
    procurarVaga(JobTitle, VacancyName, HiringManager, Status){
        cy.get(el.botaoDropdownJobTitle).click({force : true});
        cy.get(el.dropdown).contains(JobTitle).click();
        cy.get(el.botaoDropdownVacancy).click({force: true});
        cy.get(el.dropdown).contains(VacancyName).click();
        cy.get(el.botaoDropdownHiring).click({force: true});
        cy.get(el.dropdown).contains(HiringManager).click();
        cy.get(el.botaoDropdownStatus).click({force: true});
        cy.get(el.dropdown).contains(Status).click();
        cy.get(el.botaoSearch).click({force:true});
        //Se a vaga não for encontrada, a asserção passa
        cy.get('.oxd-text oxd-text--span').should('have.text', 'No Records Found');
    }

    cadstrarVaga(VacancyName, JobTitle, Description, HiringManager){
        cy.get(el.botaoAddVaga).click();
        cy.get(el.campoVacancyNameAV).type(VacancyName);
        cy.get(el.botaoSelectJobTitleAV).click();
        cy.get(el.dropdownAddVaga).contains(JobTitle).click();
        cy.get(el.campoDescriptionAV).type(Description);
        cy.get(el.campoHiringManagerAV).type(HiringManager);
        cy.wait(1200);
        cy.get(el.dropdownHiringManagerAV).contains(HiringManager).click();
        cy.get(el.botaoSaveVagaAV).click();
        //Verificar se a vaga foi add
        cy.wait(1200);
        cy.get('.--visited > .oxd-topbar-body-nav-tab-item').click();
        cy.get(':nth-child(8) > .oxd-table-row > :nth-child(2) > div').should('have.text','Test Analyst');

        
    }
    


}

export default new Recruitment();