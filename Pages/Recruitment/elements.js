//elementos das página Recruitment
export const ELEMENTS ={
    botaoPagRecruitment: ':nth-child(5) > .oxd-main-menu-item > .oxd-text',
    botaoRecruitmentVagas: ':nth-child(2) > .oxd-topbar-body-nav-tab-item',
    //elementos da página de procurar vaga
    botaoDropdownJobTitle: ':nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    dropdown: '.oxd-select-dropdown',
    botaoDropdownVacancy: ':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    botaoDropdownHiring: ':nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    botaoDropdownStatus: ':nth-child(4) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    botaoSearch: '.oxd-form-actions > .oxd-button--secondary',

    
    //elementos da página de adicionar vaga
    botaoAddVaga: '.orangehrm-header-container > .oxd-button',
    campoVacancyNameAV: '.oxd-form > :nth-child(1) > :nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-input',
    dropdownAddVaga: '.oxd-select-dropdown',
    campoDescriptionAV: '.oxd-textarea',
    campoHiringManagerAV:'.oxd-autocomplete-text-input',
    dropdownHiringManagerAV: '.oxd-autocomplete-dropdown',
    campoNumberOfPositionsAV: '.oxd-grid-2',
    botaoSaveVagaAV: '.oxd-button--secondary',
    botaoSelectJobTitleAV: '.oxd-select-text',

    //elementos da página Candidates
    dropdownCandidates: '.oxd-select-dropdown',
    botaoDropdownJobTitleC: ':nth-child(1) > .oxd-grid-4 > :nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    botaoDropdownVacancyC: ':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    botaoDropdownHiringC: ':nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    botaoDropdownStatusC: ':nth-child(4) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    campoCandidateName: '.oxd-autocomplete-text-input > input',
    dropdownCandidateName: '.oxd-autocomplete-dropdown',
    campoKeywords: ':nth-child(2) > .oxd-input',
    campoApplicationDataInitial: ':nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-date-wrapper > .oxd-date-input > .oxd-input',
    campoApplicationDataFinal: ':nth-child(4) > .oxd-input-group > :nth-child(2) > .oxd-date-wrapper > .oxd-date-input > .oxd-input',
    botaoDropdownMethod: ':nth-child(3) > .oxd-grid-4 > .oxd-grid-item > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon',
    botaoSearchCandidate: '.oxd-form-actions > .oxd-button--secondary',




}