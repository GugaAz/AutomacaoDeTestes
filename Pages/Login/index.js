//Metodos de ações da página
const el= require('./elements').ELEMENTS;

class Login {
    acessarPagina(){
        cy.visit(el.url);
    }
    realizarLogin(username, password){
        cy.clearCookie('csrftoken');
        cy.get(el.campoUsername).type(username);
        cy.get(el.campoPassword).type(password);
        cy.get(el.botaoLogin).click();
       
    }
    

}

export default new Login();