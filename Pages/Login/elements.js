//elementos das página de login
export const ELEMENTS ={
    url: "https://opensource-demo.orangehrmlive.com/",
    campoUsername: ":nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-input",
    campoPassword: ":nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-input",
    botaoLogin: ".oxd-button"

}