/// <reference types = "cypress"/>

import Leave from "../support/Desafio/Pages/Recruitment";
import Home from "../support/Desafio/Pages/Recruitment";
import Login from "../support/Desafio/Pages/Login";
import Recruitment from "../support/Desafio/Pages/Recruitment";

describe ('teste pagina Recruitment', () => {
beforeEach( () =>{
    Login.acessarPagina();
    Login.realizarLogin('Admin','admin123');
    Recruitment.acessarPaginaRecruitment();
    cy.get('.oxd-topbar-header-breadcrumb > .oxd-text').should('have.text', 'Recruitment');

})

it('acessar página de vagas', ()=>{
    Recruitment.acessarPaginaRecruitment();
    Recruitment.acessarRecruitmentVagas();
    cy.get('.oxd-table-filter-header-title > .oxd-text').should('have.text', 'Vacancies');

})

it('procurar vaga', () =>{
    Recruitment.acessarPaginaRecruitment();
    Recruitment.acessarRecruitmentVagas();
    Recruitment.procurarVaga('QA Lead', 'Senior QA Lead', 'Odis Adalwin', 'Active');

})


it('cadastrar nova vaga', ()=>{
    Recruitment.acessarPaginaRecruitment();
    Recruitment.acessarRecruitmentVagas();
    Recruitment.cadstrarVaga('Test Analyst', 'QA Engineer', 'Make a tests', 'Odis Adalwin');

})

it('procurar candidato', ()=>{
    Recruitment.acessarPaginaRecruitment();
    Recruitment.procurarCandidato('Senior QA Lead','Odis Adalwin','Application Initiated', 'Joshua', 'QA','2022-05-11', '2022-11-11', 'Manual');


})


})